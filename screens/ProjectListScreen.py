from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemableBehavior
from kivymd.uix.list import MDList

Builder.load_string("""
<ProjectListScreen>:
    MDLabel:
        text: 'ProjectListScreen'
        
    MDRaisedButton:
        text: 'go to a project'
        on_press: root.manager.current = 'project-detail'
        
    Widget:
""")


class ProjectListScreen(Screen):
    def build(self):
        print('hello')
        pass
