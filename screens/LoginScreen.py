from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemableBehavior
from kivymd.uix.list import MDList

Builder.load_string("""
<LoginScreen>
    MDFloatLayout:
        
        MDLabel:
            text: "Login"
            pos_hint: {"center_y": .85}
            font_style: "H3"
            halign: "center"
            them_text_color: "Custom"
            text_color: 0, 0, 0, 1
        
        MDLabel:
            text: "Welcome To Captiva Tool"
            pos_hint: {"center_y": .75}
            font_style: "H5"
            halign: "center"
        
        MDTextField:
            id: email
            helper_text: "There will always be a mistake"
            hint_text: "Enter Your Email"
            pos_hint: {"center_x": .5, "center_y":.6}
            current_hint_text_color: 0, 0, 0, 1
            size_hint_x: .8
        
        MDTextField:
            id: password
            hint_text: "Password"
            pos_hint: {"center_x": .5, "center_y":.45}
            current_hint_text_color: 0, 0, 0, 1
            size_hint_x: .8
            password: True
        
        MDRaisedButton:
            text: "Login"
            pos_hint: {"center_x": .5, "center_y": .25}
            on_press: root.on_login()
""")


class LoginScreen(Screen):

    def on_login(self):
        print('login')
