from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemableBehavior
from kivymd.uix.list import MDList

Builder.load_string("""
#:set menu_width 300

<ProjectMenuButton@MDRectangleFlatIconButton>
    theme_text_color: "Custom"
    text_color: 0, 0, 1, 1
    line_color: 1, 0, 1, 1
    icon_color: 1, 0, 0, 1
    

<ProjectDetailScreen>:
    MDFloatLayout:
    
        MDBoxLayout:
            id: project_menu_layout
            orientation: 'vertical'
            size_hint: (None, 1)
            width: menu_width
            
            md_bg_color: .2, .2, .2, .2
            
            ProjectMenuButton:
                icon: "android"
                text: "Image library"
                size_hint_x: 1
                
                
            ProjectMenuButton:
                icon: "android"
                text: "Image Processing Profile"
                size_hint_x: 1
                
                
            ProjectMenuButton:
                icon: "android"
                text: "Image library"
                size_hint_x: 1
                
            Widget:
                
        MDBoxLayout:
            pos:(menu_width, 0)
            size_hint: (None, 1)
            width: root.width - menu_width
            
            ScreenManager:
""")


class ProjectDetailScreen(Screen):
    def build(self):
        pass
