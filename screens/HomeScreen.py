from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemableBehavior
from kivymd.uix.list import MDList

Builder.load_string('''
<HomeMenuButton@MDRaisedButton>

<HomeScreen>:
    MDFloatLayout:
        MDRaisedButton:
            text: "Project"
            pos_hint: {"center_x": .5, "center_y": .5}
            theme_text_color: "Custom"
            text_color: 0, 0, 0, 1
            on_press: root.manager.current = 'project-list'
''')


class HomeScreen(Screen):
    def build(self):
        pass
