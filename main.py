import logging
import os

from kivy.clock import Clock
from kivymd.app import MDApp
from kivy.core.window import Window
from kivy.lang import Builder
from kivymd.uix.navigationdrawer import MDNavigationDrawer
from kivy.uix.screenmanager import ScreenManager, Screen
from home.applications.menu import MenuApplication
import screens
import components
from screens import LoginScreen
from screens.HomeScreen import HomeScreen
from screens.LoginScreen import LoginScreen

KV = '''
Screen:
    BoxLayout:
        orientation: 'vertical'

        MDToolbar:
            id: top_bar
            title: "Hello"
            right_action_items: [["menu", lambda x: slide_mav.set_state('toggle')]]
            elevation:5

        ScreenManager:
            id: home_sm
            
            HomeScreen:
                name: 'home'
            
            ProjectDetailScreen:
                name: 'project-detail'

            ProjectListScreen:
                name: 'project-list'

            LoginScreen:
                name: 'login'

    SlideBar:
        id: slide_mav
'''


class OcrToolApp(MDApp):
    def build(self):
        # prepare application configuration
        Window.size = (1000, 800)

        self.theme_cls.primary_palette = "Red"

        return Builder.load_string(KV)

    def on_start(self):
        logging.debug("starting....")

    def on_resume(self):
        print("resume")

    def on_pause(self):
        print("pause")

    def on_stop(self):
        print("stop")


if __name__ == '__main__':
    logging.basicConfig(filename='C:\\ocrtool-log\\myapp.log', level=logging.INFO)

    OcrToolApp().run()
