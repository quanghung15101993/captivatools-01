import mysql.connector
import logging
from config.MysqlCfg import MYSQL_CONST


#
#
#
class _BaseRepository:
    # instance for Database connection
    db_conn = None
    entity_name = None

    '''
        init/constructor with:
        
        @db_conn: <Object> database connection
        @entity_name: <String> database table name
    '''

    def __init__(self, db_conn, entity_name):
        if db_conn:
            self.db_conn = db_conn
            logging.debug('The %s Repository has save database connection', entity_name)

        self.entity_name = entity_name

    # def __validator(self):
    #     if self.db_conn is None:
    #         raise Exception('Db connection is none')

    '''
        function execute sql string with parameters 
        
        @sql: <String> query string
        @parameters: <Array/Tuple> parameters for execute query
        
        return cursor if success or throw exception when error
    '''

    def execute(self, sql, parameters=[]):
        with self.db_conn.cursor() as cursor:
            # executing sql query with parameter
            cursor.execute(sql, parameters)

            self.db_conn.commit()
            logging.debug('Execute query has committed')
            return cursor

    '''
        function query sql string with parameters 

        @sql: <String> query string
        @parameters: <Array/Tuple> parameters for execute query

        return object array if success or throw exception when error
    '''

    def query(self, sql, parameters=[]):
        with self.db_conn.cursor() as cursor:
            cursor.execute(sql, parameters)

            return cursor.fetchall()

    '''
        function create new entity 

        @data_object: <Object> entity data object

        return cursor if success or throw exception when error
    '''

    def create(self, data_object):
        # init Query string and parameters
        sql = 'INSERT INTO ' + self.entity_name + ' ('
        sql_params = ''
        params = []

        # preparing Sql string
        for attr, value in data_object.items():
            sql += '`' + attr + '`, '
            sql_params += '%s, '
            params.append(value)

        # remove comma & space characters
        sql = sql[:len(sql) - 2]
        sql_params = sql_params[:len(sql_params) - 2]

        # append all to complete sql string
        sql += ') VALUES (' + sql_params + ')'

        # executing
        cursor = self.execute(sql, params)
        logging.debug('Insert success with new ID = %s', cursor.lastrowid)

        return cursor

    '''
    '''

    def get_all(self):
        sql = 'SELECT * FROM ' + self.entity_name
        return self.query(sql)

    '''
    '''

    def get_by_id(self, id):
        sql = 'SELECT * FROM ' + self.entity_name + ' WHERE id = %s'
        params = (id,)

        results = self.query(sql, params)

        if results:
            return results[0]

        return None

    '''
    '''

    def delete_by_id(self, id):
        sql = 'DELETE FROM ' + self.entity_name + ' WHERE id = %s'
        params = (id,)
        self.execute(sql, params)

        logging.info('Delete object successful with ID %s', id)
