import mysql.connector
import logging
from config.MysqlCfg import MYSQL_CONST
from mydb._BaseRepository import _BaseRepository


class ProjectRepository(_BaseRepository):

    def __init__(self, mydb):
        super().__init__(mydb, entity_name='project')



if __name__ == '__main__':
    mydb = mysql.connector.connect(
        host=MYSQL_CONST['host'],
        user='root',
        password='Phuctm17!',
        database='ocr_tool',
        auth_plugin='mysql_native_password'
    )
#
    new_project = {
        'name': 'new project',
        'inactive': 1
    }
    project_repo = ProjectRepository(mydb)
    print(project_repo.create(new_project))
#     # print(project_repo.get_by_id(15))
#
#     # ProjectDB(mydb).create_project('phuctm3', '123')
#     # data = ProjectDB(mydb).get_project()
