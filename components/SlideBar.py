from kivy.lang import Builder
from kivymd.uix.navigationdrawer import MDNavigationDrawer

Builder.load_string('''
<SlideBar>:
    anchor: 'right'
    
    ContentNavigationDrawer:
        orientation: 'vertical'
        padding: '8dp'
        spacing: '8dp'

        ScrollView:
            DrawerList:
                id: md_list
                
                MDList:
                    OneLineIconListItem:
                        text: "Login"
                        on_release: app.root.ids['home_sm'].current = 'login' 

                        IconLeftWidget:
                            icon: "account-circle"

                    OneLineIconListItem:
                        text: "Home"
                        on_release: app.root.ids['home_sm'].current = 'home' 

                        IconLeftWidget:
                            icon: "home"

                    OneLineIconListItem:
                        text: "Project"
                        on_release: app.root.ids['home_sm'].current = 'project-list' 

                        IconLeftWidget:
                            icon: "file-document-box-multiple"
                    
                    OneLineIconListItem:
                        text: "Utilities"
                        on_release: app.root.ids['home_sm'].current = 'project-list' 

                        IconLeftWidget:
                            icon: "file-document-box-multiple"
''')


class SlideBar(MDNavigationDrawer):
    pass
