import requests


# culture
# LICE075-D09A-64E3
# APP3075-D09A-59C8

def print_request(request):
    print('{}\n{}\r\n{}\r\n\r\n{}'.format(
        '-----------START-----------',
        request.method + ' ' + request.url,
        '\r\n'.join('{}: {}'.format(k, v) for k, v in request.headers.items()),
        request.data,
    ))


class CaprestClient:
    url = None
    auth_ticket = None

    def __init__(self, url):
        self.url = url

    '''
        login to caprest then save authentication to self
    '''
    def login(self, username, password, license_key, application_id):
        body = {
            "username": username,
            "password": password,
            "culture": 'en-US',
            "licenseKey": license_key,
            "applicationId": application_id,
            "deviceId": "CDC-Mcrs",
            "extraAuthInfo": ""
        }

        caprest_endpoint = self.url + "/session"
        print("Captiva rest API logging in with username {username} ")

        headers = {
            "User-Agent": "OCRTool-agent",
            "Connection": "keep-alive",
            "Accept": "application/hal+json, application/vnd.emc.captiva+json",
            "Content-Type": "application/hal+json; charset=utf-8"
        }

        try:
            req = requests.Request("POST", caprest_endpoint, data=body.__str__(), headers=headers)

            req_prepared = req.prepare()

            print_request(req)

            session = requests.Session()
            response = session.send(req_prepared)

            response.raise_for_status()
            login_json = response.json()

            print("Login success")
            self.auth_ticket = login_json['ticket']
            return login_json
        except requests.exceptions.HTTPError as errh:
            print("CATCH HTTP ----- Error ----")
            print(errh)
            print(response.content)
        except requests.ConnectionError as error:
            print(error)

    '''
        check authentication 
        
    '''
    def check_authentication(self):
        if self.auth_ticket is None:
            raise Exception('Unauthorized error')

    '''
        Upload file to Captiva with current session
    '''
    def upload_file(self):
        self.check_authentication()

if __name__ == '__main__':
    caprest_client = CaprestClient('https://uatcaptiva.mbbank.com.vn/cp-rest')

    caprest_client.upload_file()
    # caprest_client.login(username='ecmadmin', password='123456bB@', license_key='LICE075-D09A-64E3', application_id='APP3075-D09A-59C8')

    print(caprest_client.auth_ticket)
